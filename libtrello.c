#include <stdio.h>
#include <curl/curl.h>
#include <stdlib.h>
#include <jansson.h>
#include <string.h>

#include "libtrello.h"

const char *key = "22cbfdcf3b388111e4d08bb8769d5ee3";
CURL *curl = NULL;
json_t *root = NULL;
json_error_t error;

//! @link http://curl.haxx.se/libcurl/c/CURLOPT_WRITEFUNCTION.html
size_t write_data(void *buffer, size_t size, size_t nmemb, void *userdata) {
    // TODO: only up to 16K bytes are returned by this function at a time.
    // TODO: If there is more than that amount of data, you need to cache and feed it all into json_loads at once.
    // TODO: data is not null terminated, make sure it is null terminated before passing to json_loads
    // TODO: can use the userdata pointer for this purpose, see http://curl.haxx.se/libcurl/c/CURLOPT_WRITEDATA.html

    // TODO: better debug logging
#ifdef DEBUG
    printf("%.*s", ((int) size) * ((int) nmemb), (char*) buffer);
    printf("\n");
#endif
    // TODO: check for empty/non-json text
    root = json_loads(buffer, 0, &error);
    return size * nmemb;
}

void deinitialize() {
    curl_easy_cleanup(curl);
    curl_global_cleanup();
}

int initialize() {
    if (curl) {
        return 0;
    }
    curl_global_init(CURL_GLOBAL_DEFAULT);
    curl = curl_easy_init();
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
    atexit(deinitialize);
    return 0;
}

json_t* get(const char *format, const char *parameter, const char *token) {
    initialize();
    char url[256];
    snprintf(url, 256, format, parameter, token, key);
    printf(format, parameter, token, key);
    printf("\n");
    curl_easy_setopt(curl, CURLOPT_URL, url);
    CURLcode status = curl_easy_perform(curl);
    if (status != CURLE_OK) {
        // TODO: better error logging
        fprintf(stderr, "libtrello: curl_easy_perform() failed: %s\n", curl_easy_strerror(status));
    }
    return root;
}

struct trello_board* get_boards(const char *token) {
    json_t *result = get("https://trello.com/1/members/%s/boards?token=%s&key=%s&filter=open&fields=name", "me", token);
    size_t num_boards = json_array_size(result);
    if (num_boards == 0) {
        json_decref(result);
        return NULL;
    }
    struct trello_board *boards = malloc(num_boards * (sizeof(struct trello_board)));
    for (size_t i = 0; i < num_boards - 1; i++) {
        json_t *board = json_array_get(result, i);
        const char *id = json_string_value(json_object_get(board, "id"));
        const char *name = json_string_value(json_object_get(board, "name"));
        strncpy(boards[i].id, id, 24);
        boards[i].name = malloc(strlen(name));
        strcpy(boards[i].name, name);
    }

    json_decref(result);
    return boards;
}

struct trello_card* get_cards(const char *token, const char *board_id) {
    json_t *result = get("https://trello.com/1/boards/%s/cards?token=%s&key=%s", board_id, token);
    size_t num_cards = json_array_size(result);
    if (num_cards == 0) {
        json_decref(result);
        return NULL;
    }
    struct trello_card *cards = malloc(num_cards * (sizeof(struct trello_card)));
    for (size_t i = 0; i < num_cards - 1; i++) {
        json_t *card = json_array_get(result, i);
        //const char *id = json_string_value(json_object_get(board, "id"));
        //const char *name = json_string_value(json_object_get(board, "name"));
        //strncpy(boards[i].id, id, 24);
        //boards[i].name = malloc(strlen(name));
        //strcpy(boards[i].name, name);
    }

    json_decref(result);
    return cards;
}
