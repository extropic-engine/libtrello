#ifndef LIBTRELLO_H
#define LIBTRELLO_H

// TODO: figure out how to allow the user to easily free struct memory

typedef struct trello_board {
    char id[24];
    char *name;
    struct trello_board *next;
} trello_board;

typedef struct trello_card {
    char id[24];
    struct trello_card *next;
} trello_card;

struct trello_boards* get_boards(const char *token);
struct trello_cards* get_cards(const char *token, const char *board_id);

#endif
